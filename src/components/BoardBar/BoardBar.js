import React from "react"
import { Container as BootstrapContainer, Row, Col } from 'react-bootstrap'
import './BoardBar.scss'

function BoardBar(){
    return (
        <nav className="navbar-board">
            <BootstrapContainer className="trello-container">
                <Row>
                    <Col sm={10} xs={12} className="col-no-padding">
                        <div className="board-info">
                            <div className="item board-logo-icon"><i className="fa fa-coffee"/>&nbsp;&nbsp;<strong>
                                Một số công cụ phát triển phần mềm</strong></div>
                            <div className="divider"></div>
                            <div className="item board-type">Private Workspace</div>
                            <div className="divider"></div>

                            <div className="item member-avatar">
                                <img src="avt1.jpg" alt="avatar-member" title="member"/>
                                <img src="avt2.jpg" alt="avatar-member" title="member"/>
                                <img src="avt3.png" alt="avatar-member" title="member"/>
                                <img src="avt1.jpg" alt="avatar-member" title="member"/>
                                <img src="code1.jpg" alt="avatar-member" title="member"/>
                                <span className="more-members">+7</span>
                                <span className="invite">Invite</span>
                            </div>
                        </div>
                    </Col>
                    <Col sm={2} xs={12} className="col-no-padding">
                        <div className="board-actions">
                            <div className="item menu"><i className="fa fa-ellipsis-h mr-2"/> &nbsp;&nbsp;Show menu</div>
                        </div>
                    </Col>
                </Row>
            </BootstrapContainer>
        </nav>
    )
}
export default BoardBar
