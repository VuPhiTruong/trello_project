/**
 *  Created by truong author on 7/11/2023
 */
//onKeydown
export const saveContentAfterPressEnter = (e) => {
    if (e.key === 'Enter'){
        e.preventDefault()
        e.target.blur()
    }
}
//select all input when click
export const selectAllInLineText = (e) => {
    e.target.focus()
    e.target.select()
    // document.execCommand('selectAll', false, null)
}