export const initialData ={
    boards: [
        {
            id: 'board-1',
            columnOrder: ['column-1', 'column-2', 'column-3', 'column-4'],
            columns: [
                {
                    id:'column-1',
                    boardId:'board-1',
                    title:'Column 1',
                    cardOrder:['card-1', 'card-2', 'card-3', 'card-4', 'card-5', 'card-7'],
                    cards: [
                        { id:'card-1', boardId:'board-1', columnId:'column-1', title:'Coding life', cover: 'code1.jpg' },
                        { id:'card-2', boardId:'board-1', columnId:'column-1', title:'Chạy bộ', cover: null },
                        { id:'card-3', boardId:'board-1', columnId:'column-1', title:'Title of card 3', cover: null },
                        { id:'card-4', boardId:'board-1', columnId:'column-1', title:'Column of table', cover: null },
                        { id:'card-5', boardId:'board-1', columnId:'column-1', title:'Title of card 5', cover: null },
                        { id:'card-7', boardId:'board-1', columnId:'column-1', title:'VU PHI TRUONG', cover: null }
                    ]
                },
                {
                    id:'column-2',
                    boardId:'board-1',
                    title:'Column 2',
                    cardOrder:['card-8', 'card-9', 'card-6', 'card-10'],
                    cards: [
                        { id:'card-8', boardId:'board-1', columnId:'column-2', title:'Column1', cover: 'code.jpg' },
                        { id:'card-9', boardId:'board-1', columnId:'column-2', title:'Tập gym', cover: null },
                        { id:'card-6', boardId:'board-1', columnId:'column-2', title:'Title of card 6', cover: null },
                        { id:'card-10', boardId:'board-1', columnId:'column-2', title:'Đá bóng', cover: null }
                    ]
                },
                {
                    id:'column-3',
                    boardId:'board-1',
                    title:'Column 3',
                    cardOrder:['card-11', 'card-12', 'card-13', 'card-14', 'card-15'],
                    cards: [
                        { id:'card-11', boardId:'board-1', columnId:'column-3', title:'Mua xe', cover: null },
                        { id:'card-12', boardId:'board-1', columnId:'column-3', title:'Avatar', cover: 'avt2.jpg' },
                        { id:'card-13', boardId:'board-1', columnId:'column-3', title:'Title of card 4', cover: null },
                        { id:'card-14', boardId:'board-1', columnId:'column-3', title:'Title of card 5', cover: null },
                        { id:'card-15', boardId:'board-1', columnId:'column-3', title:'Mua nhà', cover: null }
                    ]
                },
                {
                    id:'column-4',
                    boardId:'board-1',
                    title:'Column 4',
                    cardOrder:['card-16', 'card-17', 'card-18', 'card-19', 'card-20'],
                    cards: [
                        { id:'card-16', boardId:'board-1', columnId:'column-4', title:'ABCDEFA', cover: null },
                        { id:'card-17', boardId:'board-1', columnId:'column-4', title:'Avatar', cover: 'php1.jpg' },
                        { id:'card-18', boardId:'board-1', columnId:'column-4', title:'Title of card 4', cover: null },
                        { id:'card-19', boardId:'board-1', columnId:'column-4', title:'Title of card 5', cover: null },
                        { id:'card-20', boardId:'board-1', columnId:'column-4', title:'Mua nhà', cover: null }
                    ]
                }
            ]
        }
    ]
}